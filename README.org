A simple package that uses [[https://github.com/Alexander-Miller/treemacs][treemacs]] to display a nicely-structured view of a JSON document.

* Install
Note that this package requires that your Emacs is compiled with support for JSON parsing.

I personally use [[https://github.com/raxod502/straight.el][straight.el]] for managing my dependencies:

#+BEGIN_SRC emacs-lisp
  (use-package treemacs-json
    :straight (:host gitlab :repo "bricka/emacs-treemacs-json")
    :commands treemacs-json)
#+END_SRC

* How to Use
Very simple: just open a buffer containing valid JSON and run the command ~treemacs-json~.

* How it Works
Under the hood, it uses [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Parsing-JSON.html][Emacs's JSON-parsing abilities]] to parse the JSON in the buffer, and then uses ~treemacs~ as a framework to construct a tree-based hierarchy.


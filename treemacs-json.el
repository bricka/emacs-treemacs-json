;;; treemacs-json.el --- Display JSON with Treemacs  -*- lexical-binding: t; -*-

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Version: 0.1
;; Package-Requires: ((emacs "27.0") (treemacs "2.9") (ht "2.3"))

;;; Commentary:

;;; Code:

(require 'treemacs)
(require 'ht)

(defvar treemacs-json--parsed-json nil
  "The parsed JSON to be rendered with `treemacs-json'.

Unfortunately, there is no good way to pass external input into a
Treemacs extension, so we need to use an intermediate
variable.")

(treemacs-define-leaf-node treemacs-json-string-leaf
  (treemacs-get-icon-value 'tag-leaf))

(treemacs-define-leaf-node treemacs-json-number-leaf
  (treemacs-get-icon-value 'tag-leaf))

(defun treemacs-json--node-label (key label)
  "Return the label for LABEL.  If KEY, then prepend with it."
  (if key
      (concat key ": " label)
    label)
  )

(defmacro treemacs-json--render (includes-key)
  "How to render an item in `treemacs-json'.  If INCLUDES-KEY, item is a list with a key in the first slot."
  `(let ((val (if ,includes-key (cadr item) item))
         (label (when ,includes-key (car item))))
     (cond
      ((stringp val)
       (treemacs-render-node
        :icon treemacs-treemacs-json-string-leaf-icon
        :label-form (treemacs-json--node-label label (format "%S" val))
        :state treemacs-treemacs-json-string-leaf-state
        :face 'font-lock-string-face
        :key-form item
        )
       )
      ((numberp val)
       (treemacs-render-node
        :icon treemacs-treemacs-json-number-leaf-icon
        :label-form (treemacs-json--node-label label (number-to-string val))
        :state treemacs-treemacs-json-number-leaf-state
        :face 'font-lock-string-face
        :key-form item
        )
       )
      ((listp val)
       (treemacs-render-node
        :icon treemacs-icon-treemacs-json-array-closed
        :label-form (treemacs-json--node-label label (treemacs-json--preview-array val))
        :state treemacs-treemacs-json-array-closed-state
        :face 'font-lock-string-face
        :key-form "array"
        :more-properties (:content val)
        )
       )
      ((hash-table-p val)
       (treemacs-render-node
        :icon treemacs-icon-treemacs-json-object-closed
        :label-form (treemacs-json--node-label label (treemacs-json--preview-object val))
        :state treemacs-treemacs-json-object-closed-state
        :face 'font-lock-string-face
        :key-form "object"
        :more-properties (:content val)
        ))
      )
     ))

(defun treemacs-json--preview-thing (thing)
  "Produce a JSON preview string for THING."
  (cond
   ((stringp thing) (if (> (length thing) 10)
                        (format "%S" (concat (substring thing 0 7) "..."))
                      (format "%S" thing)))
   ((numberp thing) (number-to-string thing))
   ((listp thing) (if thing "[...]" "[]"))
   ((hash-table-p thing) (if (hash-table-empty-p thing) "{}" "{...}"))
   )
  )

(defun treemacs-json--preview-array (array)
  "Generate a string preview of ARRAY."
  (if (not array) "[]"
    (concat
     "[ "
     (string-join
      (mapcar #'treemacs-json--preview-thing (-take 5 array))
      ", "
      )
     (if (> (length array) 5) ", ... ]" " ]")
     )
    ))

(treemacs-define-expandable-node treemacs-json-array
  :icon-open (treemacs-as-icon "[] " 'face 'font-lock-type-face)
  :icon-closed (treemacs-as-icon "[] " 'face 'font-lock-type-face)
  :query-function (treemacs-button-get node :content)
  :render-action
  (treemacs-json--render nil)
  )

(defun treemacs-json--preview-object (object)
  "Generate a string preview of OBJECT."
  (if (hash-table-empty-p object) "{}"
      (concat
       "{ "
       (string-join
        (mapcar
         (lambda (p) (concat (car p) ": " (treemacs-json--preview-thing (cdr p))))
         (sort (-take 5 (ht->alist object)) (lambda (p1 p2) (string< (car p1) (car p2)))))
        ", ")
       (if (> (hash-table-count object) 5) ", ... }" " }")
      )
  ))

(treemacs-define-expandable-node treemacs-json-object
  :icon-open (treemacs-as-icon "{} " 'face 'font-lock-type-face)
  :icon-closed (treemacs-as-icon "{} " 'face 'font-lock-type-face)
  :query-function (sort (ht-items (treemacs-button-get node :content)) (lambda (p1 p2) (string< (car p1) (car p2))))
  :render-action
  (treemacs-json--render t)
  )

(treemacs-define-expandable-node treemacs-json-root
  :icon-open (treemacs-get-icon-value 'root-open)
  :icon-closed (treemacs-get-icon-value 'root-closed)
  :query-function (list treemacs-json--parsed-json)
  :render-action
  (treemacs-json--render nil)
  :top-level-marker t
  :root-label "ROOT"
  :root-face 'font-lock-type-face
  :root-key-form 'json
  )


(defun treemacs-json ()
  "Display the current JSON buffer as a Treemacs tree."
  (interactive)
  (setq treemacs-json--parsed-json (json-parse-string (buffer-string) :array-type 'list))
  (switch-to-buffer "*treemacs-json*")
  (treemacs-initialize)
  (treemacs-TREEMACS-JSON-ROOT-extension)
  )

(provide 'treemacs-json)
;;; treemacs-json.el ends here
